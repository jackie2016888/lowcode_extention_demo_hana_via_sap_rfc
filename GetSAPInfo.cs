﻿using GrapeCity.Forguncy.ServerApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json;
using SapNwRfc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GetSAPInfo.DataSchema;

namespace GetSAPInfo
{
    public class GetSAPInfo:ForguncyApi
    {
        private string _postjson;
        private string _result;

        [Post]
        public void CallRFCFunction()
        {
            try
            {
                var syncIOFeature = Context.Features.Get<IHttpBodyControlFeature>();
                if (syncIOFeature != null)
                {
                    syncIOFeature.AllowSynchronousIO = true;
                }
                using (StreamReader reader = new StreamReader(Context.Request.Body))
                {
                    _postjson = reader.ReadToEnd();
                }
                var headersObj = Context.Request.Headers;

                var _connectionString = headersObj["ConnectionString"];
                var _RFCFunction = headersObj["RFCFunction"];

                using (var connection = new SapConnection(_connectionString))
                {
                    connection.Connect();
                    switch (_RFCFunction)
                    {
                        case "ZLIFNR_CREATE":
                            //单条记录添加
                            using (var someFunction = connection.CreateFunction("ZLIFNR_CREATE"))
                            {
                                var createVendorParametersObj = JsonConvert.DeserializeObject<CreateVendorParameters>(_postjson);
                                _result = JsonConvert.SerializeObject(someFunction.Invoke<CreateVendorResult>(createVendorParametersObj));
                            }
                            break;
                        case "ZLIFNR_SEARCH":
                            //查询返回多条记录
                            using (var someFunction = connection.CreateFunction("ZLIFNR_SEARCH"))
                            {
                                var getVendorInfoParametersObj = JsonConvert.DeserializeObject<GetVendorInfoParameters>(_postjson);
                                _result = JsonConvert.SerializeObject(someFunction.Invoke<GetVendorInfoResult>(getVendorInfoParametersObj));
                            }
                            break;
                        case "Z_OI_MM_PR_CREATE":
                            //主子表数据添加
                            using (var someFunction = connection.CreateFunction("Z_OI_MM_PR_CREATE"))
                            {

                                var createPurchaseOrderParametersObj = JsonConvert.DeserializeObject<CreatePurchaseOrderParameters>(_postjson);
                                _result = JsonConvert.SerializeObject(someFunction.Invoke<CreatePurchaseOrderResult>(createPurchaseOrderParametersObj));
                            }
                            break;
                    }
                    this.Context.Response.WriteAsync(_result).Wait();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
